# Ostrich Run #

Description on http://gamejolt.com/games/other/ostrich-run/27774/

Survive a ferocius cheetah and run for as long as possible in this endless runner MiniGame. 

Controls:

- Press Keyboard Down Key to eat food underground.

- Press Space to trigger a Speed power Up.

 Don't eat too much or you'll lose a life. 
 

Credits: 

Originally meant to be an entry for Ludum dare 29, with theme "Beneath the surface". Unfortunately, we ran out of time, but we completed this later.

Art: Andrea Vela

Programming: Mauricio Labarca

Music: "Fun in a bottle" by Kevin McCleod :

http://incompetech.com/

Keyboard buttons art made by Xelu licensed under Public Domain:

http://opengameart.org/users/xelu

 Any other resources made with Unity, Uitoolkit, Gimp, Shoebox, Sfxr, etc.

### What is this repository for? ###

This repository includes all files needed to open this up in Unity as a project folder. This was made in Unity 4.5 using a modified version of Uitoolkit (It has a fix for webplayer buttons)

Currently A lot of the code can be refactored or made into re-usable scripts.


Contact: 
Mauricio Labarca, bitbucket username: mlbarca 

