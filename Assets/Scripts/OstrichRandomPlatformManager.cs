﻿using UnityEngine;
using System.Collections.Generic;

public class OstrichRandomPlatformManager : MonoBehaviour {

	public GameObject[] prefabs;
	public int numberOfObjects;
	public float recycleOffset;
	public Vector3 startPosition;

	// This variable controls the distribution that will 
	// calculate the spawn chance of each platform
	public float[] PlatformProbs = {85f, 10f, 5f};


	private Vector3 nextPosition;
	private Vector3 outOfSightPosition;
	private Queue<GameObject> objectQueue;
	private DistRandom<GameObject> distribution;
	private GameObject powerUpBlock;
	private GameObject eggBlock;
	private GameObject reserveBlock;
	private bool powerUpInQueue = false;

	public float sueloWidth = 0.8f;
	void Start () {

		// Start a new queue, fill it and instantiate.
		objectQueue = new Queue<GameObject>(numberOfObjects);
		nextPosition = startPosition;


		for (int i = 0; i < numberOfObjects; i++) {

			// Instantiates initial platforms at the start
			objectQueue.Enqueue((GameObject)Instantiate(prefabs[0], nextPosition, Quaternion.identity));
			nextPosition += new Vector3(sueloWidth, 0f, 0f);


		}
		enabled = true;

		outOfSightPosition = new Vector3(-20f, 20f, 0f);

		// Spawn the power ups out of sight
		powerUpBlock = Instantiate(prefabs[1], outOfSightPosition, Quaternion.identity) as GameObject;
		eggBlock = Instantiate(prefabs[2], outOfSightPosition, Quaternion.identity) as GameObject;


		// creating a Probability distribution object chooses the next platform based on their %.
		double[] probabilities = {(double)PlatformProbs[0], (double)PlatformProbs[1], (double)PlatformProbs[2]};
		distribution = new DistRandom<GameObject>(prefabs, probabilities);
	}

	void Update () {

		// move the platforms.
		foreach (GameObject platform in objectQueue){
			platform.transform.localPosition -= new Vector3(OstrichControl.internalVelocity * Time.deltaTime, 
			                                                0f, 0f);   
		}

		// Check if large earth component in the leftmost platform is not visible.
		bool visible = true;
		foreach (Renderer render in objectQueue.Peek().GetComponentsInChildren<Renderer>() ){
			visible = render.isVisible;
		}

		// Recycle the platform depending on its type.
		if(!visible){

			nextPosition = new Vector3(objectQueue.Peek().transform.localPosition.x + 
			                           sueloWidth * numberOfObjects , nextPosition.y, nextPosition.z);

			// If there is currently a powerup in the queue, check if this is a 
			// power up by counting children
			if (powerUpInQueue){
				if(objectQueue.Peek().transform.childCount == 0){
					Recycle();
				} else { 
					RecycleIntoNormal();
				}
			} else {

				// If not, call the random distribution manager and check if we should spawn a powerup
				GameObject selected = distribution.Next();
				if (selected.transform.childCount != 0){

					// IF the distribution manager selected a powerup, turn this normal platform into one
					RecycleIntoPowerUp(selected);
				} else{

					// iF Not, Get this to the end of the line
					Recycle();
				}

			}

		}
	}

	/// <summary>
	/// Recycle the leftmost platform by changing its position and moving it to the end of queue
	/// </summary>
	private void Recycle () {

		Vector3 position = nextPosition;

		GameObject o = objectQueue.Dequeue();
		o.transform.localPosition = position;
		objectQueue.Enqueue(o);

	}

	/// <summary>
	/// Move the current block to a reserve by moving it out of sight, and add the correct powerup
	/// to the object queue.
	/// </summary>
	/// <param name="selected">Selected.</param>
	private void RecycleIntoPowerUp (GameObject selected) {
		
		Vector3 position = nextPosition;
		
		reserveBlock = objectQueue.Dequeue();
		reserveBlock.transform.localPosition = outOfSightPosition;

		if(selected == prefabs[1]){
			objectQueue.Enqueue(powerUpBlock);
			powerUpBlock.transform.localPosition = position;

		} else{
			objectQueue.Enqueue(eggBlock);
			eggBlock.transform.localPosition = position;
		}

		powerUpInQueue = true;

		
	}

	/// <summary>
	///  Turn the not visible power up into a normal
	/// </summary>
	private void RecycleIntoNormal () {

		objectQueue.Dequeue().transform.localPosition = outOfSightPosition;
		
		reserveBlock.transform.localPosition = nextPosition;
		objectQueue.Enqueue(reserveBlock);
		
		powerUpInQueue = false;
	}



	private void GameStart () {
		nextPosition = startPosition;

		enabled = true;
	}

	private void GameOver () {
		enabled = false;
	}
}