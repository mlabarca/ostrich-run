﻿using UnityEngine;
using System.Collections;

public class SuperiorPlaformAction : MonoBehaviour {

	Animator anim;

	// Use this for initialization
	void Awake () {
		anim = gameObject.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if(!gameObject.renderer.isVisible){
			anim.SetBool("Eaten", false);
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (OstrichControl.v < 0){
			anim.SetBool("Eaten", true);
		}

	}
}
