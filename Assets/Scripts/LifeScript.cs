﻿using UnityEngine;
using System.Collections;

public class LifeScript : MonoBehaviour {

	public float textSize480px = 0.5f;

	// Offset of lives from text in percentage of Screen Width
	public float lifeOffsetPerc = 0.015f;
	private float lifeTextXoffset;

	// Variables to hold and calculate the size of life sprite.
	private Vector3 lifeSpriteSize;
	private float lifeSpriteWidth;
	private float lifeSpriteHeight;
	public int lifeAmount = 3;
	private Vector3 lifeStartPosition;
	private UITextInstance LifesText;
	public GameObject lifeSpritePrefab;


	private Stack lifeStack;

	public Animator OstrichAnim;




	// Use this for initialization
	void Start () {
		// Setting up UI text font
		UIText font = new UIText("font", "font.png");
		font.alignMode = UITextAlignMode.Left;

		// Initial text instance. Text size depends on screen width
		LifesText = font.addTextInstance("Lifes : ", Screen.width / 3, 
		                                 Screen.height * 0.95f,  Screen.height* textSize480px/ 480f , 0);

        //LifesText.pixelsFromLeft((int) (Screen.width/4  - LifesText.width/2));
		LifesText.positionFromBottomLeft(0.09f, 0.0875f);
		LifesText.refreshPosition();


		// Sprite stack
		lifeStack = new Stack(lifeAmount);

		lifeSpriteSize = lifeSpritePrefab.GetComponent<SpriteRenderer>().sprite.bounds.size;
		lifeSpriteWidth = lifeSpriteSize.x * lifeSpritePrefab.transform.localScale.x;
		lifeSpriteHeight = lifeSpriteSize.y * lifeSpritePrefab.transform.localScale.y;


		// Get the start positioning of lives relative to the text.
		lifeStartPosition = Camera.main.ScreenToWorldPoint( new Vector3(LifesText.xPos + LifesText.width, 
		                                                                LifesText.yPos + Screen.height, 0));
		// Calculate an x offset based on screen width values.
		lifeTextXoffset = - Camera.main.orthographicSize * ((float)Screen.width/Screen.height) * 2 * lifeOffsetPerc;

		// Create lifes and push their renderers into the stack. Lifes position in Y corrected. Positioning
		// independent of screen sizes.
		for (int i = 0; i < lifeAmount; i++){

			Vector3 lifePos = new Vector3(lifeStartPosition.x + lifeTextXoffset + lifeSpriteWidth * i, 
			                              lifeStartPosition.y - lifeSpriteHeight/2f, lifeStartPosition.z);
			lifeStack.Push ( Instantiate(lifeSpritePrefab, lifePos, Quaternion.identity) as GameObject);
		}

		//OstrichAnim = GameObject.FindGameObjectWithTag("Ostrich").GetComponent<Animator>();


	}
	
	// Update is called once per frame
	void Update () {

	
	}

	/// <summary>
	/// Removes an amount lifes from players
	/// </summary>
	/// <param name="amount">Amount of lifes to remove .</param>
	public void TakeLife(int amount){

		OstrichAnim.SetTrigger("Blink");


		// Check if we have enough lifes and remove 1 by 1
		if (lifeStack.Count >= amount){
			for (int i = 0; i < amount; i++){
				GameObject life = (GameObject)lifeStack.Pop();
				Destroy(life);
			}
		} 
		if (lifeStack.Count == 0){
			audio.Play();
			PersistentScript.persistentScore = OstrichScore.score;
			Application.LoadLevel("Ostrich Game Over");
		}

	}

	/// <summary>
	/// Gives One life to player
	/// </summary>
	public void GiveLife (){



		if (lifeStack.Count < lifeAmount){

			//Calculate new life positions
			GameObject topLife = (GameObject)lifeStack.Peek();
			Vector3 lifePos = new Vector3(topLife.transform.position.x + lifeSpriteWidth,
			                              lifeStartPosition.y - lifeSpriteHeight/2, 
			                              lifeStartPosition.z);
			GameObject life = Instantiate(lifeSpritePrefab, lifePos, Quaternion.identity) as GameObject;

			// Push new life into the life stack
			lifeStack.Push(life);
		}


	}
}
