﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Score script increases scoring rate  when the screen is touched
/// </summary>
public class OstrichScore : MonoBehaviour {


	/// <summary>
	/// Current score
	/// </summary>
	public static float score;

	/// <summary>
	/// Score increase when player lets loose
	/// </summary>
	public int multiplier;
	
	// Amount of seconds it takes to increase score by 1
	public float increaseFreq = 1f; 

	// The text size you would want if screen height is 480px,
	// Which is a medium screen height
	// See http://developer.android.com/guide/practices/screens_support.html#testing
	public float textSize480px = 0.5f;

	// The score text instance
	private UITextInstance scoreText;

	// 1  unity unit = unitsTodistanceRatio Meters
	public float unitsToDistanceRatio = 25f;
 
	
	/// <summary>
	/// Resets the statics Variables.
	/// </summary>
	void resetStatics(){
		score = 0f;
	}

	// Use this for initialization
	void Start () {
		resetStatics();

		// Setting up UI text font
		UIText font = new UIText("font", "font.png");
		font.alignMode = UITextAlignMode.Center;

		// Initial text instance. Text size depends on screen width
		scoreText = font.addTextInstance(score.ToString(), Screen.width / 2, 
		            Screen.height * 0.95f,  Screen.height* textSize480px/ 480f , 0);

		// Changing position
		scoreText.positionFromBottom( 0.001f);
		scoreText.text = "Just a test";
	}
	
	// Update is called once per frame
	void Update () {
		score = OstrichControl.distanceTraveled/unitsToDistanceRatio;

		// Format the text for 1 decimal.
		scoreText.text = string.Format("{0:0.0}", score)  + " m";

	}
	

}
