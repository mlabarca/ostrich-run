﻿using UnityEngine;
using System.Collections;

public class HelpUI : MonoBehaviour {

	UIButton menuBt;

	public float buttonScale720p = 0.8f;
	private float buttonScale;

	public float SpaceFromBot = 0.005f;

	public float textScale720p = 0.5f;
	private float insTextScale;


	private UIText font;

	// Use this for initialization
	void Start () {

		buttonScale = Screen.height * buttonScale720p/ 720f;
		insTextScale = Screen.height * textScale720p / 720f;

		Debug.Log(buttonScale);
		font = new UIText("font", "font.png");

		UITextInstance runAndEat = font.addTextInstance ("RUN AND EAT FROM THE GROUND FOR AS LONG AS YOU CAN", 0, 0);
		runAndEat.positionFromTop(0.25f);
		runAndEat.alignMode = UITextAlignMode.Center;
		runAndEat.textScale = insTextScale * 0.8f;

		UITextInstance fruitText = font.addTextInstance("Eat     to run faster and avoid", 0, 0);
		fruitText.positionFromTop(0.35f);
		fruitText.alignMode = UITextAlignMode.Center;
		fruitText.textScale = insTextScale;

		UITextInstance eggText = font.addTextInstance("Eat     to recover lives", 0, 0);
		eggText.positionFromTop(0.45f);
		eggText.alignMode = UITextAlignMode.Center;
		eggText.textScale = insTextScale;

		UITextInstance stomachText = font.addTextInstance("Do not fill your stomach past 100 or you will lose 1", 0, 0);
		stomachText.positionFromTop(0.55f);
		stomachText.alignMode = UITextAlignMode.Center;
		stomachText.textScale = insTextScale * 0.9f;


		UITextInstance keyDownText = font.addTextInstance("Press     to eat", 0, 0);
		keyDownText.positionFromTop(0.65f);
		keyDownText.alignMode = UITextAlignMode.Center;
		keyDownText.textScale = insTextScale;

		UITextInstance keySpaceText = font.addTextInstance("Press     to activate", 0, 0);
		keySpaceText.positionFromTop(0.75f);
		keySpaceText.alignMode = UITextAlignMode.Center;
		keySpaceText.textScale = insTextScale;



		// go back to menu button
		menuBt = UIButton.create("MainMenu.png", "MainMenup.png", 0, 0);
		menuBt.scale = new Vector3(buttonScale, buttonScale);
		menuBt.positionFromBottom(SpaceFromBot);
		menuBt.onTouchUpInside += MenuAction;

	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void MenuAction (UIButton sender){
		audio.Play();
		Application.LoadLevel("Ostrich Menu");
	}
}

