﻿using UnityEngine;
using System.Collections;

public class LeopardoScript : MonoBehaviour {


	public float leopardoMaxXPos;
	public float leopardoInitialSpeed;

	public float minLeopardoSpawnTime;
	public float maxLeopardoSpawnTime;

	public float leopardoScoreProp = 0.005f;
	public float leopardoSpeed;

	// Left limit of screen
	private float leftLimit;

	// Calculated  x start position, off screen
	private float leopardoStartXPos;


	// Use this for initialization
	void Start () {
	
		// This formula calculates the position that delimits the screen in world units.
		leftLimit = (-1.0f) * Camera.main.orthographicSize * ((float)Screen.width/Screen.height);

		// Calculate exact startpos based on previous formula.
		leopardoStartXPos = leftLimit - gameObject.GetComponent<SpriteRenderer>().bounds.size.x;

		StartCoroutine(LeopardoRun());
		
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.parent.localPosition.x <= leopardoStartXPos){
			gameObject.renderer.enabled = true;
		}

		leopardoSpeed = leopardoInitialSpeed + OstrichScore.score * leopardoScoreProp;

	}
	

	/// <summary>
	/// Controls the running motion of the cheetah.
	/// </summary>
	/// <returns>The run.</returns>
	private IEnumerator LeopardoRun(){

		// Run this forever 
		while (true) {

			// Wait, before running for the first time or running again.
			yield  return new WaitForSeconds(Random.Range(minLeopardoSpawnTime, maxLeopardoSpawnTime));

			// Increase pos until we reach target, using current delta time
			while (transform.parent.position.x < leopardoMaxXPos) {
				Vector3 leopardoPos = 
					new Vector3 (leopardoMaxXPos, transform.parent.position.y, transform.parent.position.z);
				transform.parent.position = 
					Vector3.MoveTowards (transform.parent.position, leopardoPos, leopardoSpeed * Time.deltaTime);

				// Go back and update frame, get me a fresh time.deltatime, and come back here
				yield return null;
			}

			// Now decrease pos until we return to initial pos.
			while (transform.parent.position.x > leopardoStartXPos) {
				Vector3 leopardoPos = new Vector3 (leopardoStartXPos, transform.parent.position.y, transform.parent.position.z);
				transform.parent.position = 
					Vector3.MoveTowards (transform.parent.position, leopardoPos, leopardoSpeed * Time.deltaTime); 

				// Go back and update frame, get me a fresh time.deltatime, and come back here
				yield return null;
			}
		}

	}



	void OnTriggerEnter2D (Collider2D other){
		if (other.tag == "Ostrich"){
			audio.Play();
			gameObject.renderer.enabled = false;
		}

	}
}
