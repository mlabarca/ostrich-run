﻿using UnityEngine;
using System.Collections.Generic;

public class OstrichPlatformManager : MonoBehaviour {

	public GameObject prefab;
	public int numberOfObjects;
	public float recycleOffset;
	public Vector3 startPosition;
	
//	public Material[] materials;
//	public PhysicMaterial[] physicMaterials;
	//public Booster booster;

	private Vector3 nextPosition;
	private Queue<GameObject> objectQueue;


	public float sueloWidth = 0.8f;
	void Start () {


		objectQueue = new Queue<GameObject>(numberOfObjects);
		//GameStart();
		nextPosition = startPosition;
		for (int i = 0; i < numberOfObjects; i++) {

			objectQueue.Enqueue((GameObject)Instantiate(prefab, nextPosition, Quaternion.identity));
			nextPosition += new Vector3(sueloWidth, 0f, 0f);

		}
		enabled = true;
	}

	void Update () {

		foreach (GameObject platform in objectQueue){
			platform.transform.localPosition -= new Vector3(OstrichControl.internalVelocity * Time.deltaTime, 
			                                                0f, 0f);   
		}
		//objectQueue.Peek().transform.localPosition.x + recycleOffset < OstrichControl.distanceTraveled)
		if(!objectQueue.Peek().renderer.isVisible){
			nextPosition = new Vector3(objectQueue.Peek().transform.localPosition.x + 
			                           sueloWidth * numberOfObjects , nextPosition.y, nextPosition.z);
			Recycle();
		}
	}

	private void Recycle () {

		Vector3 position = nextPosition;
		//position.x += sueloWidth * 0.5f;
		//position.y += sueloWidth * 0.5f;
		//booster.SpawnIfAvailable(position);

		GameObject o = objectQueue.Dequeue();
		//o.localScale = scale;
		o.transform.localPosition = position;
//		int materialIndex = Random.Range(0, materials.Length);
//		o.renderer.material = materials[materialIndex];
//		o.collider.material = physicMaterials[materialIndex];
		objectQueue.Enqueue(o);

		//nextPosition += new Vector3(sueloWidth, 0, 0);

//		if(nextPosition.y < minY){
//			nextPosition.y = minY + maxGap.y;
//		}
//		else if(nextPosition.y > maxY){
//			nextPosition.y = maxY - maxGap.y;
//		}
	}
	
	private void GameStart () {
		nextPosition = startPosition;
//		for(int i = 0; i < numberOfObjects; i++){
//			Recycle();
//		}
		enabled = true;
	}

	private void GameOver () {
		enabled = false;
	}
}