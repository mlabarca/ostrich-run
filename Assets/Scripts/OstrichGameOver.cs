﻿using UnityEngine;
using System.Collections;

public class OstrichGameOver : MonoBehaviour {

	UIButton retryBt, menuBt, quitBt;

	// Button spacing values
	public float buttonScale720p = 0.8f;
	public float buttonScale;
	public int buttonSpacing = 6;
	public float LayoutSpaceFromBot = 0.005f;

	// Text Spacing Options
	public float scoreTextSpaceFromBot = 0.35f;
	public float scoreTextScale720p = 0.7f;
	private float scoreTextScale;
	public float highScoreTextSpaceFromBot = 0.25f;
	private float highScoreTextScale;
	public float gameOverTextSpace = 0.3f;
	public float gameOverTextScale = 1f;

	private UIText font;
	private float currentScore;
	private float currentHighScore;
	private bool webplayer = false;

	// Use this for initialization
	void Start () {

		font = new UIText("font", "font.png");
		buttonScale = Screen.height * buttonScale720p / 720f;
		scoreTextScale = Screen.height * scoreTextScale720p / 720f;
		highScoreTextScale = scoreTextScale * 0.9f;

		// Game over text
//		UITextInstance GameOverText = font.addTextInstance("Game Over!", 0f, 0f);
//		GameOverText.positionCenter();
//		GameOverText.positionFromTop(gameOverTextSpace);
//		GameOverText.textScale = gameOverTextScale;

		currentScore = PersistentScript.persistentScore;

		// Set score text
		UITextInstance scoreText = font.addTextInstance("You ran for " + 
		                                                string.Format("{0:0.0}", currentScore) +" meters", 0f, 0f, 1f, 0);
		scoreText.positionCenter();
		scoreText.positionFromBottom(scoreTextSpaceFromBot);
		scoreText.textScale = scoreTextScale;


		// Get the current highscore and compare, save if a new high score is obtained
		currentHighScore = PlayerPrefs.GetFloat("Current Player");
		if(currentScore > currentHighScore ){
			currentHighScore = currentScore;
			PlayerPrefs.SetFloat("Current Player", currentHighScore);
		}

		UITextInstance highScoreText = font.addTextInstance("Your highest is " + 
		                                                    string.Format("{0:0.0}",currentHighScore) +" meters", 0f, 
		                                                    0f, 1f, 0);
		highScoreText.positionCenter();
		highScoreText.positionFromBottom(highScoreTextSpaceFromBot);
		highScoreText.textScale = highScoreTextScale;


		// Retry button
		retryBt = UIButton.create("Retry.png", "Retryp.png", Screen.width/2, Screen.height/2);
		retryBt.scale = new Vector3(buttonScale, buttonScale);
		retryBt.onTouchUpInside += retryAction;

		// go back to menu button
		menuBt = UIButton.create("MainMenu.png", "MainMenup.png", 0, 0);
		menuBt.scale = new Vector3(buttonScale, buttonScale);
		menuBt.onTouchUpInside += MenuAction;


		// If running on webplayer, don't show the quit button.
		#if UNITY_WEBPLAYER
			webplayer = true;
		
		#endif

		if (!webplayer){
			// Quit
			quitBt = UIButton.create("Quit.png", "Quitp.png", 0, 0);
			quitBt.scale = new Vector3(buttonScale, buttonScale);
			quitBt.onTouchUpInside += QuitAction;
		}



		// Lay all of these vertically
		var vBox = new UIVerticalLayout( buttonSpacing );
		if(!webplayer){
			vBox.addChild(retryBt, menuBt, quitBt);
		} else {
			vBox.addChild(retryBt, menuBt);
		}



		vBox.matchSizeToContentSize();

		// Position at center of screen.
		vBox.positionCenter();
		vBox.positionFromBottom(LayoutSpaceFromBot);

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// Start game
	/// </summary>
	/// <param name="sender">Sender button</param>
	public void retryAction(UIButton sender){
		audio.Play();
		Application.LoadLevel("Ostrich run");
	}

	/// <summary>
	/// Quit game
	/// </summary>
	/// <param name="sender">Sender button.</param>
	public void QuitAction(UIButton sender){
		audio.Play();
		Application.Quit();
	}

	public void MenuAction (UIButton sender){
		audio.Play();
		Application.LoadLevel("Ostrich Menu");
	}
}
