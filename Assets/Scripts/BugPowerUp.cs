﻿using UnityEngine;
using System.Collections;

public class BugPowerUp : MonoBehaviour {

	public float textSize480px = 0.5f;
	
	// Offset of bug powerups from text in percentage of Screen Width
	public float bugOffsetPerc = 0.015f;
	private float bugTextXoffset;

	private float bugSpriteWidth;
	private float bugSpriteHeight;
	private Vector3 bugSpriteSize;
	private Vector3 bugStartPosition;

	//Maximum amount of bug power ups the player can have
	public int bugAmount = 3;
	private UITextInstance bugText;
	public GameObject bugSpritePrefab;
	
	[HideInInspector]
	public static Stack bugStack;





	// Use this for initialization
	void Start () {

		// Setting up UI text font
		UIText font = new UIText("font", "font.png");
		font.alignMode = UITextAlignMode.Left;
		
		// Initial text instance. Text size depends on screen width
		bugText = font.addTextInstance("Power ups : ", 0, 0, Screen.height* textSize480px/ 480f , 0);

		bugText.positionFromBottomLeft(0.15f, 0.01f);
		//bugText.positionFromTopRight(0.01f, 0.05f);

		// Bug stack
		bugStack = new Stack(bugAmount);

		bugSpriteSize = bugSpritePrefab.GetComponent<SpriteRenderer>().sprite.bounds.size;
		bugSpriteWidth = bugSpriteSize.x * bugSpritePrefab.transform.localScale.x;
		bugSpriteHeight = bugSpriteSize.y * bugSpritePrefab.transform.localScale.y;
		
		// Get the start positioning of lives relative to the text.
		bugStartPosition = Camera.main.ScreenToWorldPoint( new Vector3(bugText.xPos + bugText.width, 
		                                                                bugText.yPos + Screen.height, 0));
		// Calculate an x offset based on screen width values.
		bugTextXoffset = - Camera.main.orthographicSize * ((float)Screen.width/Screen.height) * 2 * bugOffsetPerc;

	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public bool TakeBug(int amount){
		
		bool success;
		
		
		// Check if we have enough lifes and remove 1 by 1
		if (bugStack.Count >= amount){
			for (int i = 0; i < amount; i++){
				GameObject bug = (GameObject)bugStack.Pop();
				Destroy(bug);
			}
			success = true;
			return success;
		} else {
			success = false;
			return success;
		} 
	}

	public void GiveBug (){

		if (bugStack.Count < bugAmount){
			
			//Calculate new life positions
			Vector3 bugPos;
			if(bugStack.Count == 0){
				bugPos = new Vector3(bugStartPosition.x + bugTextXoffset, bugStartPosition.y - bugSpriteHeight/2, -10);

			} else {
				GameObject topBug = (GameObject)bugStack.Peek();
				bugPos = new Vector3(topBug.transform.position.x + bugSpriteWidth, 
				                             bugStartPosition.y - bugSpriteHeight/2, -10);
			}

			GameObject bug = Instantiate(bugSpritePrefab, bugPos, Quaternion.identity) as GameObject;	

			// Push new life into the life stack
			bugStack.Push(bug);
		}
		
		
	}
}
