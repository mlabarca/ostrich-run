﻿using UnityEngine;
using System.Collections;

public class BugScript : MonoBehaviour {



	private bool canSpeedUp = true;
	private float leftLimit;
	private BugPowerUp bugManagerInstance;


	// Use this for initialization
	void Start () {

		// This formula calculates the position that delimits the screen in world units.
		leftLimit = (-1.0f) * Camera.main.orthographicSize * ((float)Screen.width/Screen.height);

		bugManagerInstance = GameObject.FindGameObjectWithTag("Settings").GetComponent<BugPowerUp>();
	}
	
	// Update is called once per frame
	void Update () {

		// Check if outside screen and if its not showing, then enable it
		if ( gameObject.transform.position.x < leftLimit - gameObject.renderer.bounds.size.x 
		    && !gameObject.renderer.enabled){
			gameObject.renderer.enabled = true;
			canSpeedUp = true;
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (OstrichControl.v <  0 && canSpeedUp){
			audio.Play();
			gameObject.renderer.enabled = false;
			//OstrichControl.activateSpeed = true;
			bugManagerInstance.GiveBug();
			canSpeedUp = false;
		}
	
	}
}
