﻿using UnityEngine;
using System.Collections;

public class OstrichMenu : MonoBehaviour {

	UIButton startBt, helpBt, creditsBt, quitBt;

	public float buttonScale720p = 0.8f;
	private float buttonScale;
	public int buttonSpacing = 6;
	public float LayoutSpaceFromBot = 0.005f;
	private bool webPlayer = false;

	// Use this for initialization
	void Start () {

		// Change the scale depending on screensize
		buttonScale = Screen.height * buttonScale720p / 720f;


		// Start button
		startBt = UIButton.create("Start.png", "Startp.png", Screen.width/2, Screen.height/2);
		startBt.scale = new Vector3(buttonScale, buttonScale);
		startBt.onTouchUpInside += StartAction;

		// Help button
		helpBt = UIButton.create("Help.png", "Helpp.png", 0, 0);
		helpBt.scale = new Vector3(buttonScale, buttonScale);
		helpBt.onTouchUpInside += HelpAction;

		// Credits
		creditsBt = UIButton.create("Credits.png", "Creditsp.png", 0, 0);
		creditsBt.scale = new Vector3(buttonScale, buttonScale);
		creditsBt.onTouchUpInside += CreditsAction;

		#if UNITY_WEBPLAYER
			webPlayer = true;

		#endif
		if (!webPlayer){
			// Quit
			quitBt = UIButton.create("Quit.png", "Quitp.png", 0, 0);
			quitBt.scale = new Vector3(buttonScale, buttonScale);
			quitBt.onTouchUpInside += QuitAction;
		}


		// Lay all of these vertically
		var vBox = new UIVerticalLayout( buttonSpacing );
		if(!webPlayer){
			vBox.addChild(startBt, helpBt, creditsBt, quitBt);
		} else {
			vBox.addChild(startBt, helpBt, creditsBt);
		}

		vBox.matchSizeToContentSize();

		// Position at center of screen.
		vBox.positionCenter();
		vBox.positionFromBottom(LayoutSpaceFromBot);

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// Start game
	/// </summary>
	/// <param name="sender">Sender button</param>
	public void StartAction(UIButton sender){
		audio.Play();
		Application.LoadLevel("Ostrich run");
	}

	/// <summary>
	/// Quit game
	/// </summary>
	/// <param name="sender">Sender button.</param>
	public void QuitAction(UIButton sender){
		audio.Play();
		Application.Quit();
	}

	public void HelpAction(UIButton sender){
		audio.Play();
		Application.LoadLevel("Ostrich Help");
	}

	public void CreditsAction(UIButton sender){
		audio.Play();
		Application.LoadLevel("Ostrich Credits");
	}


}
