﻿using UnityEngine;
using System.Collections;

public class CreditsUI : MonoBehaviour {

	UIButton menuBt;

	public float buttonScale720p = 0.8f;
	public float buttonScale;
	public float SpaceFromBot = 0.005f;



	// Use this for initialization
	void Start () {

		buttonScale = Screen.height * buttonScale720p / 720f;

		// go back to menu button
		menuBt = UIButton.create("MainMenu.png", "MainMenup.png", 0, 0);
		menuBt.scale = new Vector3(buttonScale, buttonScale);
		menuBt.positionFromBottom(SpaceFromBot);
		menuBt.onTouchUpInside += MenuAction;

	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void MenuAction (UIButton sender){
		audio.Play();
		Application.LoadLevel("Ostrich Menu");
	}
}

