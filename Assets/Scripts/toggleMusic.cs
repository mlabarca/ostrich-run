﻿using UnityEngine;
using System.Collections;

public class toggleMusic : MonoBehaviour {

	private AudioSource source;

	public float buttonScale720p = 0.6f;
	private float buttonScale;
	private UIToggleButton musicToggle;
	private bool musicOn = true;



	// Use this for initialization
	void Start () {

		if (GameObject.FindGameObjectWithTag("music") != null) {
			source = MusicManager.Instance.source;
		} else  {
			this.enabled = false;
			
		}

		buttonScale = Screen.height * buttonScale720p / 720f;
		
		// Music on /off toggle button
		musicToggle = UIToggleButton.create("musica_on.png", "musica_off.png", "musica_off.png", 0, 0);
		musicToggle.positionFromTopRight(0.02f, 0.01f);
		musicToggle.scale = new Vector3(buttonScale, buttonScale);
		musicToggle.onToggle += (sender, selected) => musicOn = !selected;
	
	}
	
	// Update is called once per frame
	void Update () {
		if (!musicOn && !source.mute){
			source.mute = true;
		} else if (musicOn && source.mute){
			source.mute = false;

		}
	}
}
