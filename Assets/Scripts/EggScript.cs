﻿using UnityEngine;
using System.Collections;

public class EggScript : MonoBehaviour {

	LifeScript currentLifeScript;
	SpriteRenderer eggSpriteRenderer;

	private bool canGiveLife = true;
	private float leftLimit;

	// Use this for initialization
	void Start () {
		currentLifeScript = GameObject.FindGameObjectWithTag("Settings").GetComponent<LifeScript>();
	

		// This formula calculates the position that delimits the screen in world units.
		leftLimit = (-1.0f) * Camera.main.orthographicSize * ((float)Screen.width/Screen.height);
	}
	
	// Update is called once per frame
	void Update () {

		if ( gameObject.transform.position.x < leftLimit - gameObject.renderer.bounds.size.x 
		    && !gameObject.renderer.enabled){
			gameObject.renderer.enabled = true;
			canGiveLife = true;
		}

	
	}

	void OnTriggerEnter2D(Collider2D other){

		//eggSpriteRenderer.enabled = false;


		if (OstrichControl.v <  0 && canGiveLife){
			audio.Play();
			gameObject.renderer.enabled = false;
			currentLifeScript.GiveLife();
			canGiveLife = false;
		}


		
	}
}
