﻿using UnityEngine;
using System.Collections;

public class MainCameraFollow : MonoBehaviour {

	private float lastDistance = 0;
	private float deltaDistance;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		deltaDistance = OstrichControl.distanceTraveled - lastDistance;
		lastDistance = OstrichControl.distanceTraveled;
		transform.position += new Vector3(deltaDistance, 0, 0);
	}
}
