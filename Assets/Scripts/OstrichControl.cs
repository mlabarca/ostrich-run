﻿using UnityEngine;
using System.Collections;

public class OstrichControl : MonoBehaviour {

	// Normal ostrich speed
	public float normalSpeed = 1f;

	// Max ostrich speed
	public float maxSpeed = 2f;

	// ostrich positions
	public float OstrichStartPos = -4.2f;
	public float OstrichMaxPos = 1.8f;
	public float OstrichPowerUpSpeed = 5f;

	// Enemy speed decrease Penalty %
	public float speedPenalty =50;

	// Speed the stomach fill increases
	public float fillSpeed = 10f;

	// text size
	public float textSize480px = 1.6f;

	public Camera scrollingCamera;
	private scrollingV2 scrollScript;

	// Ratio between background speed and front images scroll speed 
	public float ParallaxRatio = 0.02f;
	
	private Animator anim;
	private bool poweredUp = false;
	private UIText font;
	private UITextInstance stomachText;
	private LifeScript lifeInstance;
	private BugPowerUp bugManagerInstance;
	private bool leopardoHit = false;


	[HideInInspector]
	public static float distanceTraveled;
	public static float internalVelocity;
	public static float v = 0;
	public static bool activateSpeed = false;
	public static float stomachFillAmount = 0f;
	[HideInInspector]
	private Vector3 startPosition;


	/// <summary>
	/// Resets the static variables to their corresponding values,
	/// Since they tend to keep values when loading other scenes.
	/// This could be better handled with delegates.
	/// </summary>
	void ResetStatics(){
		distanceTraveled = 0f;
		internalVelocity = 0;
		v = 0;
		activateSpeed = false;
		stomachFillAmount = 0f;
	}


	void Awake() 
	{
		ResetStatics();

		internalVelocity = normalSpeed;
		startPosition = transform.localPosition;
		anim = gameObject.GetComponent<Animator>();
		lifeInstance = GameObject.FindGameObjectWithTag("Settings").GetComponent<LifeScript>();
		bugManagerInstance = GameObject.FindGameObjectWithTag("Settings").GetComponent<BugPowerUp>();

	} 
	// Use this for initialization
	void Start () {
		UIText font = new UIText("font", "font.png");
		font.alignMode = UITextAlignMode.Center;
		
		// Initial text instance. Text size depends on screen width
		stomachText = font.addTextInstance("Stomach: ", Screen.width * 2f / 3f, 
		                                 Screen.height * 0.95f,  Screen.height* textSize480px/ 480f , 0);
		stomachText.positionFromBottomRight (0.02f, 0.05f);

		scrollScript = scrollingCamera.GetComponentInChildren<scrollingV2>();

	
	}
	
	// Update is called once per frame
	void Update () {

		gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x, startPosition.y,
		                                                 gameObject.transform.localPosition.z);
		v = Input.GetAxis ("Vertical");


		// Fill stomach if eating
		if (v < 0){
			anim.SetBool("Eating", true);

			if(stomachFillAmount < 100f){
				stomachFillAmount += Time.deltaTime * fillSpeed;
			} else {

				//Play clip
				audio.Play();

				// Take a life if stomach is full and reset it
				lifeInstance.TakeLife(1);
				stomachFillAmount = 0f;
			}
		}else {
			anim.SetBool("Eating", false);

			// Empty Stomach
			if(stomachFillAmount > 0f){
				stomachFillAmount -= Time.deltaTime * fillSpeed/2f;

				if (stomachFillAmount < 0f){
					stomachFillAmount = 0f;
				}
			}
		}



		// This easing increases Decreases of color gradually until it reaches red. Since it should
		// decrease while stomachfilleAmount increases, we negate result and pad the function.
		float currentRedAndGreen =(-Easing.Sinusoidal.easeIn(stomachFillAmount/100f) + 1f);

		// Update stomach text
		stomachText.text = "Stomach: " + Mathf.FloorToInt(stomachFillAmount);
		stomachText.color = new Color(1f , currentRedAndGreen, currentRedAndGreen, 1f);




		if (Input.GetKeyUp(KeyCode.Space) && !activateSpeed){
			bool bugSucess = bugManagerInstance.TakeBug(1);
			if (bugSucess){
				activateSpeed = true;
			}

		}





		// Run power up position change if a speed power up has been eaten
		if (activateSpeed){
			RunPowerUp(Time.deltaTime);
			internalVelocity = maxSpeed;
		}else{
			internalVelocity = normalSpeed;
		}

		scrollScript.speed = internalVelocity * ParallaxRatio;

		distanceTraveled += Time.deltaTime * internalVelocity;

		}

	
	public void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Leopardo" && !leopardoHit){
			lifeInstance.TakeLife(1);
			leopardoHit = true;
		}

	}

	public void OnTriggerExit2D(Collider2D other){
		if (other.tag == "Leopardo"){
			leopardoHit = false;
		}

	}



	public void RunPowerUp(float deltaTime){
		if (!poweredUp){
			transform.parent.position = Vector3.MoveTowards(transform.parent.position, 
			                                                new Vector3(OstrichMaxPos,
			            									transform.parent.position.y, transform.parent.position.z), 
			                                                OstrichPowerUpSpeed * deltaTime);  
		} else if ( transform.parent.position.x >= OstrichStartPos){
			transform.parent.position = Vector3.MoveTowards(transform.parent.position, 
			                                                new Vector3(OstrichStartPos, 
			            									transform.parent.position.y, transform.parent.position.z), 
			                                                OstrichPowerUpSpeed * deltaTime);  
			//gameObject.transform.localPosition += new Vector3(-6f * Time.deltaTime, 0f);
		} 	
		if (transform.parent.position.x >= OstrichMaxPos){
			poweredUp = true;
			//gameObject.rigidbody2D.velocity = new Vector2(-6f, 0f);
		}		
		
		if (poweredUp && transform.parent.position.x <= OstrichStartPos){
			poweredUp = false;
			activateSpeed = false;
		}

	}


}
