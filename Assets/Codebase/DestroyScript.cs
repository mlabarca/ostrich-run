﻿using UnityEngine;
using System.Collections;

public class DestroyScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D (Collider2D thing) {
 		if (thing.tag == "Player"){
			Debug.Break();
			return;
		}

		if (thing.gameObject.transform.parent && gameObject.tag != "lower limit"){
			Destroy (thing.gameObject.transform.parent.gameObject);
		}
		else if(gameObject.tag != "lower limit"){ 
			Destroy (thing.gameObject);
		}
	}
}
