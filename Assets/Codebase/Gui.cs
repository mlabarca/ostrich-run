﻿using UnityEngine;
using System.Collections;

public class Gui : MonoBehaviour {
	public float rectangleWidth; 
	public float rectangleHeight;

	private PlayerControl player;
	private float vSpeed;

	// Use this for initialization
	void Start () {
	
		
		// IMPORTANT: depth is 1 on top higher numbers on the bottom.  This means the lower the number is the closer it gets to the camera.
		var playButton = UIButton.create( "playUp.png", "playDown.png", 0, 0 );
		//playButton.scale = new Vector3(0.1f,0.1f,0.1f);
		playButton.positionFromTopLeft( 0.01f, 0f );
		playButton.highlightedTouchOffsets = new UIEdgeOffsets( 30 );
		playButton.onTouchUpInside += ( sender ) => Debug.Log( "clicked the button: " + sender );



	}
	
	// Update is called once per frame
	void Update () {
	
	}

//	void OnGUI(){
//		player = GameObject.Find ("megaman").GetComponent<PlayerControl> ();
//		vSpeed = player.rigidbody2D.velocity.y; 
//		GUI.Label (new Rect (transform.position.x, transform.position.y, rectangleWidth, rectangleHeight), vSpeed.ToString());
//
//	}

}
