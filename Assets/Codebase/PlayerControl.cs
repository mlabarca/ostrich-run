﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {

	[HideInInspector]
	public bool facingRight = true;			// For determining which way the player is currently facing.
	[HideInInspector]
	public bool jump = false;				// Condition for whether the player should jump.

	public float moveForce = 1f;
	public float jumpForce = 20f;
	public float maxSpeed = 5f;
	public float groundRadious = 3f;
	public LayerMask whatIsground;

	private Transform groundCheck;
	private Animator anim;
	private scrollingV2 scroll;
	private bool grounded = false;


	float h = 0;

	void Awake() 
	{
		groundCheck = transform.Find ("groundcheck");
		anim = GetComponent<Animator> ();
		scroll = GameObject.Find ("ScrollingCamera").GetComponentInChildren<scrollingV2> (); 
	} 
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (grounded && Input.GetKeyDown (KeyCode.Space)) 
		{
			rigidbody2D.AddForce (Vector2.up * jumpForce);
			anim.SetBool("ground",false);
		}
		 h = Input.GetAxis ("Horizontal");

		if (h > 0) {
//						scroll.direction = 1f;			
//						scroll.enabled = true;
			scroll.toTheRight = true;
			scroll.TurnOn();
			} else if (h < 0) {
//						scroll.direction = -1f;			
//						scroll.enabled = true;
			scroll.toTheRight = false;
			scroll.TurnOn();
			} else
			scroll.TurnOff();
						

	
		}

	void FixedUpdate () {


		grounded = Physics2D.Linecast (transform.position, groundCheck.position, whatIsground);
		//grounded = Physics2D.OverlapCircle (groundCheck.position, groundRadious, whatIsground);
		if (gameObject.rigidbody2D.velocity.y >= 0){
		anim.SetBool("ground",grounded);		
		}


		anim.SetFloat ("vSpeed", rigidbody2D.velocity.y);

		//float h = Input.GetAxis ("Horizontal");
		//if (Mathf.Abs(h) > 0) 
		//	scroll.Scroll (h);
		anim.SetFloat ("speed", Mathf.Abs(h));

		// If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxSpeed yet...
		if(h * rigidbody2D.velocity.x < maxSpeed)	
			// ... add a force to the player.
			rigidbody2D.velocity = new Vector2(h * maxSpeed, rigidbody2D.velocity.y);
			//rigidbody2D.AddForce(Vector2.right * h * moveForce);

		// If the player's horizontal velocity is greater than the maxSpeed...
		if(Mathf.Abs(rigidbody2D.velocity.x) > maxSpeed)
			// ... set the player's velocity to the maxSpeed in the x axis.
			rigidbody2D.velocity = new Vector2(Mathf.Sign(rigidbody2D.velocity.x) * maxSpeed, rigidbody2D.velocity.y);

		if (h > 0 && !facingRight)
						flip ();
		if (h < 0 && facingRight)
						flip ();



	}

	void flip(){
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
		facingRight = !facingRight;
	}

	void OnTriggerEnter2D(Collider2D other){

		if (other.gameObject.tag == "platform" && gameObject.rigidbody2D.velocity.y < 0){
			anim.SetBool("ground", true);
		}
	}


}
